var items  = document.querySelectorAll('.color');

var format = "hex";
var paletteName = "basic";

var ID = ['clip', 'hex-btn', 'rgb-btn', 'change-btn', 'drop-btn', 'dm'];
var store = [];

for(var x = 0; x< ID.length; x++){
    store.push(document.getElementById(ID[x]));
}


store[4].addEventListener('click', function(){
        store[5].classList.toggle('drop-menu');
});


drawColor();

store[3].addEventListener('click', function(){

        if(paletteName == "basic"){
            paletteName = "french";
            drawColor();
        }else{
            paletteName = "basic";
            drawColor();
        }

});

store[1].addEventListener('click', function(){
    if(format === "hex") return;
    format = "hex";
    changeType();
    this.classList.remove('btn-light');
    this.classList.add('btn-dark');
    store[2].classList.remove('btn-dark');
    store[2].classList.add('btn-light');
});

store[2].addEventListener('click', function(){
    if(format === "rgb") return;
    format = "rgb";
    changeType();
    this.classList.remove('btn-light');
    this.classList.add('btn-dark');
    store[1].classList.remove('btn-dark');
    store[1].classList.add('btn-light');
});


function drawColor(){
    for(var x = 0; x < items.length; x++){

        items[x].style.backgroundColor = palette[paletteName][x+1].hex;
        items[x].name = palette[paletteName][x+1][format];
        

        items[x].addEventListener('click', function(){
            store[0].value = this.name;
            store[0].select();
            document.execCommand('copy');
        });
}
}


function changeType(){
    for (let x = 0; x < items.length; x++) {
        items[x].name = palette[paletteName][x+1][format]; 
    }
}