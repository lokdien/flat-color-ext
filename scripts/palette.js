var palette = {
    basic: {
        1: {
            hex: "#1abc9c",
            rgb: "rgb(26, 188, 156)" },
        2: {
            hex:"#2ecc71",
            rgb: "rgb(46, 204, 113)"},
        3: {
            hex :"#3498db",
            rgb: "rgb(52, 152, 219)"},
        4: {
            hex :"#9b59b6",
            rgb: "rgb(155, 89, 182)"},
        5: {
            hex :"#34495e",
            rgb: "rgb(52, 73, 94)"},


        6: { 
            hex: "#16a085",
            rgb: "rgb(22, 160, 133)"},
        7: { 
            hex: "#27ae60",
            rgb: "rgb(39, 174, 96)"},
        8: {
            hex: "#2980b9",
            rgb: "rgb(41, 128, 185)"},
        9: {
             hex: "#8e44ad",
             rgb: "rgb(142, 68, 173)"},
        10: {
             hex: "#2c3e50",
             rgb: "rgb(44, 62, 80)"},
        

        11: {
             hex: "#f1c40f",
             rgb: "rgb(241, 196, 15)"},
        12: { 
             hex: "#e67e22",
             rgb: "rgb(230, 126, 34)"},
        13: {
             hex: "#e74c3c",
             rgb: "rgb(231, 76, 60)"},
        14: {
             hex: "#ecf0f1",
             rgb: "rgb(236, 240, 241)"},
        15: {
             hex: "#95a5a6",
             rgb: "rgb(149, 165, 166)"},


        16: {
             hex: "#f39c12",
             rgb: "rgb(243, 156, 18)"},
        17: {
             hex: "#d35400",
             rgb: "rgb(211, 84, 0)"},
        18: {
             hex: "#c0392b",
             rgb: "rgb(192, 57, 43)"},
        19: {
             hex: "#bdc3c7",
             rgb: "rgb(189, 195, 199)"},
        20: {
             hex: "#7f8c8d",
             rgb: "rgb(127, 140, 141)"}
    },
    french: {
        1: {
            hex: "#fad390",
            rgb: "rgb(250, 211, 144)" },
        2: {
            hex:"#f8c291",
            rgb: "rgb(248, 194, 145)"},
        3: {
            hex :"#6a89cc",
            rgb: "rgb(106, 137, 204)"},
        4: {
            hex :"#82ccdd",
            rgb: "rgb(130, 204, 221)"},
        5: {
            hex :"#b8e994",
            rgb: "rgb(184, 233, 148)"},


        6: { 
            hex: "#f6b93b",
            rgb: "rgb(246, 185, 59)"},
        7: { 
            hex: "#e55039",
            rgb: "rgb(229, 80, 57)"},
        8: {
            hex: "#4a69bd",
            rgb: "rgb(74, 105, 189)"},
        9: {
             hex: "#60a3bc",
             rgb: "rgb(96, 163, 188)"},
        10: {
             hex: "#78e08f",
             rgb: "rgb(120, 224, 143)"},
        

        11: {
             hex: "#fa983a",
             rgb: "rgb(250, 152, 58)"},
        12: { 
             hex: "#eb2f06",
             rgb: "rgb(235, 47, 6)"},
        13: {
             hex: "#1e3799",
             rgb: "rgb(30, 55, 153)"},
        14: {
             hex: "#3c6382",
             rgb: "rgb(60, 99, 130)"},
        15: {
             hex: "#38ada9",
             rgb: "rgb(56, 173, 169)"},


        16: {
             hex: "#e58e26",
             rgb: "rgb(229, 142, 38)"},
        17: {
             hex: "#b71540",
             rgb: "rgb(183, 21, 64)"},
        18: {
             hex: "#0c2461",
             rgb: "rgb(12, 36, 97)"},
        19: {
             hex: "#0a3d62",
             rgb: "rgb(10, 61, 98)"},
        20: {
             hex: "#079992",
             rgb: "rgb(7, 153, 146)"}
    }
};
